<?php
    namespace App\Interfaces\Customer;
    interface CustomerBookingRepositoryInterface{
        public function create();
        public function update();
        public function list();
    }
?>
